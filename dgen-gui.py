#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  dgen-gui.py
#
#  Copyright 2018 Franco Geller <franco.geller@frakaft.com.ar>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Pango, GdkPixbuf
from game import Game
from data import columns, rows

dgen="%s/%s"%(os.path.dirname(__file__),"./dgen-sdl-1.33/dgen")
g_rom_dir="%s/%s"%(os.path.dirname(__file__),"ROM/")
g_bmp_dir="%s/%s"%(os.path.dirname(__file__),"BMP/")

game = Game()

def delete_event(window, event):
    Gtk.main_quit()

def button_release(button, event):
    os.system("%s %s"%(dgen, game.get()))

def row_activate(tree_view, path, column):
    os.system("%s %s"%(dgen, game.get()))

def about(self, item):
        aboutdialog = Gtk.AboutDialog()
        aboutdialog.set_program_name("DGEN GUI")
        aboutdialog.set_version("alpha")
        aboutdialog.set_comments("A simple user interface for the DGEN emulator")
        aboutdialog.set_license_type(Gtk.License.GPL_2_0)
        aboutdialog.set_copyright("© 2018 Franco Geller")
        aboutdialog.set_authors(["Franco Geller"])
        aboutdialog.set_website_label("Git Developer Website")
        aboutdialog.set_website("http://github.com/frakaft")
        aboutdialog.set_logo(GdkPixbuf.Pixbuf.new_from_file("%s/%s"%(os.path.dirname(__file__),"dgen-logo.svg")))
        aboutdialog.show()

def cancel_config(button, event):
    print("cancel")

def save_config(button, event):
    print("save")

def select_row(selection, dic):
    try:
        num = selection.get_selected_rows()[1][0]
        name = selection.get_selected_rows()[0][num][0]
        code = selection.get_selected_rows()[0][num][1]
        ext = selection.get_selected_rows()[0][num][2]
        flags = selection.get_selected_rows()[0][num][3]
        dic["image"].set_from_file("%s%s.%s"%(
                g_bmp_dir if g_bmp_dir[-1:] == '/' else g_bmp_dir+'/',
                code,
                "BMP"
        ))
        dic["title"].set_markup("<span size='small'>%d/%d</span>"%(
                int(str(num))+1,
                len(rows)
        ))
        dic["text"].set_markup("<span size='large' weight='bold'>%s</span>\n<span size='small'>%s.%s</span>"%(
                name.replace("&","&amp;"),
                code.replace("&","&amp;"),
                ext
        ))
        game.set(g_rom_dir if g_rom_dir[-1:] == '/' else g_rom_dir+'/', code, ext, flags)
        print("(%d) '%s' %s.%s %s"%(int(str(num))+1, name, code, ext, flags))
    except IndexError:
        pass

def main():
    builder = Gtk.Builder()
    builder.add_from_file("%s/%s"%(os.path.dirname(__file__),"gui.glade"))
    main_window = builder.get_object("main_window")
    main_window.set_title("DGEN GUI")
    main_window.set_icon(GdkPixbuf.Pixbuf.new_from_file("%s/%s"%(os.path.dirname(__file__),"dgen-icon.svg")))
    main_window.set_size_request(900, 600)
    handlers = {
        "on_main_window_delete_event": delete_event,
        "on_quit_button_release_event": delete_event,
        "on_button_button_release_event": button_release,
        "on_help_menu_activate_current": about,
    }
    builder.connect_signals(handlers)

    builder.get_object("button").add(Gtk.Image.new_from_file("%s/%s"%(os.path.dirname(__file__),"run-icon.svg")))
    builder.get_object("button").set_size_request(20,20)

    dic = {}
    for i in ["scrolled_window","image","title","text"]:
        dic[i] = builder.get_object(i)

    # default values
    dic["image"].set_from_file("%s/%s"%(os.path.dirname(__file__),"dgen.svg"))
    dic["title"].set_markup("<span size='small'>~/%d</span>"%(len(rows)))
    dic["text"].set_markup("<span size='large' weight='bold'>Choose a game!</span>")

    # the data in the model (three strings for each row, one for each column)
    listmodel = Gtk.ListStore(str, str, str, str)
    # append the values in the model
    for i in range(len(rows)):
        listmodel.append(rows[i])

    # a treeview to see the data stored in the model
    view = Gtk.TreeView(model=listmodel)
    # for each column
    for i in range(len(columns)):
        # cellrenderer to render the text
        cell = Gtk.CellRendererText()
        # the text in the first column should be in boldface
        if i == 0:
            cell.props.weight_set = True
            cell.props.weight = Pango.Weight.BOLD
        # the column is created
        col = Gtk.TreeViewColumn(columns[i], cell, text=i)
        # and it is appended to the treeview
        view.append_column(col)

    # when a row is selected, it emits a signal
    view.get_selection().connect("changed", select_row, dic)
    view.connect("row-activated", row_activate)

    dic["scrolled_window"].add(view)

    main_window.show_all()

    Gtk.main()

if __name__ == '__main__':
    main()

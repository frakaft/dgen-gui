# -*- coding: utf-8 -*-
#
#  dgen-gui.py
#
#  Copyright 2018 Franco Geller <franco.geller@frakaft.com.ar>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

class Game():

    def __init__(self):
        self.rom_dir = ""
        self.code = ""
        self.ext = ""
        self.flags = ""

    def set(self, rom_dir, code, ext, flags):
        self.rom_dir = rom_dir
        self.code = code
        self.ext = ext
        self.flags = flags

    def get(self):
        return ("%s %s%s.%s"%(self.flags, self.rom_dir, self.code, self.ext))

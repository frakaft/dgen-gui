# -*- coding: utf-8 -*-
#
#  data.py
#
#  Copyright 2018 Franco Geller <franco.geller@frakaft.com.ar>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

columns = ["Game", "Code", "EXT", "FLAGS"]

rows = [["1607 A.D. Adventure","1607","BIN","-R U -g 1"],
["NONE","2DUDES","SMD","-R U -g 1"],
["688 Attack Sub","688-ASUB","SMD","-R U -g 1"],
["NONE","ACROBAT2","SMD","-R U -g 1"],
["NONE","ACROBAT","SMD","-R U -g 1"],
["NONE","ACTION52","SMD","-R U -g 1"],
["NONE","ADAISE","SMD","-R U -g 1"],
["NONE","ADDAMS","SMD","-R U -g 1"],
["NONE","AEROBIZ","SMD","-R U -g 1"],
["NONE","AEROB","SMD","-R U -g 1"],
["NONE","AFTERBR2","BIN","-R U -g 1"],
["NONE","AIRDIVER","SMD","-R U -g 1"],
["NONE","AIRWOLF","SMD","-R U -g 1"],
["NONE","ALADDIN","BIN","-R U -g 1"],
["NONE","ALESTE","SMD","-R U -g 1"],
["NONE","ALEXKID","SMD","-R U -g 1"],
["NONE","ALIEN3","SMD","-R U -g 1"],
["NONE","ALIENSOL","SMD","-R U -g 1"],
["NONE","ALIENSTM","SMD","-R U -g 1"],
["NONE","ALISIAD","BIN","-R U -g 1"],
["NONE","ANIMAN","SMD","-R U -g 1"],
["NONE","APOSSUM","BIN","-R U -g 1"],
["NONE","AQUATICG","SMD","-R U -g 1"],
["NONE","ARCADE","SMD","-R U -g 1"],
["NONE","ARCHRIVA","SMD","-R U -g 1"],
["NONE","ARCPM97","SMD","-R U -g 1"],
["NONE","ARTFIGHT","SMD","-R U -g 1"],
["NONE","ATOMICR","SMD","-R U -g 1"],
["NONE","AWORLD","BIN","-R U -g 1"],
["NONE","BACKF3","BIN","-R U -g 1"],
["NONE","BALLJACK","SMD","-R U -g 1"],
["NONE","BARNEY","SMD","-R U -g 1"],
["NONE","BARTNITE","SMD","-R U -g 1"],
["NONE","BARTSPAC","SMD","-R U -g 1"],
["NONE","BATLMSTR","BIN","-R U -g 1"],
["NONE","BATMAN2","SMD","-R U -g 1"],
["NONE","BATMAN3","SMD","-R U -g 1"],
["NONE","BATMAN4","SMD","-R U -g 1"],
["NONE","BATMAN","SMD","-R U -g 1"],
["NONE","BATNROB","SMD","-R U -g 1"],
["NONE","BATTLEFR","SMD","-R U -g 1"],
["NONE","BBALL3","BIN","-R U -g 1"],
["NONE","BB-ROAR","SMD","-R U -g 1"],
["NONE","BDOUG","SMD","-R U -g 1"],
["NONE","BEAST","SMD","-R U -g 1"],
["NONE","BEAUTY_B","SMD","-R U -g 1"],
["NONE","BEAVIS&B","SMD","-R U -g 1"],
["NONE","BERENSTN","SMD","-R U -g 1"],
["NONE","BESTOFBE","SMD","-R U -g 1"],
["NONE","B_FRENZY","SMD","-R U -g 1"],
["NONE","BINIMI","SMD","-R U -g 1"],
["NONE","BIOHZRD","BIN","-R U -g 1"],
["NONE","BLASTER2","SMD","-R U -g 1"],
["NONE","BLOCKOUT","BIN","-R U -g 1"],
["NONE","BOASIS","SMD","-R U -g 1"],
["NONE","BOB","SMD","-R U -g 1"],
["NONE","BODCOUNT","SMD","-R U -g 1"],
["NONE","BOOGRMAN","SMD","-R U -g 1"],
["NONE","BRUTAL","SMD","-R U -g 1"],
["NONE","BTANK","BIN","-R U -g 1"],
["NONE","BTECH","BIN","-R U -g 1"],
["NONE","BTLSQUAD","SMD","-R U -g 1"],
["NONE","BTOADS","BIN","-R U -g 1"],
["NONE","BTOADSDD","BIN","-R U -g 1"],
["NONE","B_TOUR96","SMD","-R U -g 1"],
["NONE","BUBSTIX","SMD","-R U -g 1"],
["NONE","BUBSY2","SMD","-R U -g 1"],
["NONE","BUBSY","BIN","-R U -g 1"],
["NONE","BUCKRODG","BIN","-R U -g 1"],
["NONE","BULVSLAK","SMD","-R U -g 1"],
["NONE","BUNNY","SMD","-R U -g 1"],
["NONE","B_WALSH","BIN","-R U -g 1"],
["NONE","CADASH","BIN","-R U -g 1"],
["NONE","CAL50","SMD","-R U -g 1"],
["NONE","CALGAMES","SMD","-R U -g 1"],
["NONE","CASTILLU","SMD","-R U -g 1"],
["NONE","CASTLEVN","BIN","-R U -g 1"],
["NONE","CEASARS","BIN","-R U -g 1"],
["NONE","CENTURIO","SMD","-R U -g 1"],
["NONE","CENTY","SMD","-R U -g 1"],
["NONE","C_FOOT97","SMD","-R U -g 1"],
["NONE","CHAKAN","BIN","-R U -g 1"],
["NONE","CHAOS2","BIN","-R U -g 1"],
["NONE","CHESTER","BIN","-R U -g 1"],
["NONE","CHUCK2","SMD","-R U -g 1"],
["NONE","CHUCK","BIN","-R U -g 1"],
["NONE","CLAYFTR","SMD","-R U -g 1"],
["NONE","COLUMNS3","SMD","-R U -g 1"],
["NONE","COLUMNS","BIN","-R U -g 1"],
["NONE","COMBATCA","SMD","-R U -g 1"],
["NONE","COMIXZN","SMD","-R U -g 1"],
["NONE","CONTRA","SMD","-R U -g 1"],
["NONE","COOLSPOT","SMD","-R U -g 1"],
["NONE","C_POOL","BIN","-R U -g 1"],
["NONE","CRACK","SMD","-R U -g 1"],
["NONE","C_RIPKIN","SMD","-R U -g 1"],
["NONE","CRUEBALL","BIN","-R U -g 1"],
["NONE","CUP90","SMD","-R U -g 1"],
["NONE","CUP92","SMD","-R U -g 1"],
["NONE","CURSE","BIN","-R U -g 1"],
["NONE","CWC_SOCC","BIN","-R U -g 1"],
["NONE","CYBORGJ","SMD","-R U -g 1"],
["NONE","DARIUS2","SMD","-R U -g 1"],
["NONE","DARKCAST","SMD","-R U -g 1"],
["NONE","DASHIN","SMD","-R U -g 1"],
["NONE","DAVEROBN","BIN","-R U -g 1"],
["NONE","DBZ","SMD","-R U -g 1"],
["NONE","DDMAUI","SMD","-R U -g 1"],
["NONE","DDUEL","SMD","-R U -g 1"],
["NONE","DD-WAR","SMD","-R U -g 1"],
["NONE","DECAPATK","SMD","-R U -g 1"],
["NONE","DEMOLMAN","SMD","-R U -g 1"],
["NONE","DES-DEMO","SMD","-R U -g 1"],
["NONE","DESERTS","SMD","-R U -g 1"],
["NONE","DEVILCR","SMD","-R U -g 1"],
["NONE","DEVILSH","BIN","-R U -g 1"],
["NONE","DHEAD","SMD","-R U -g 1"],
["NONE","DJBOY","SMD","-R U -g 1"],
["NONE","DOOMTRPR","SMD","-R U -g 1"],
["NONE","DRACULA","BIN","-R U -g 1"],
["NONE","DRAGON2","BIN","-R U -g 1"],
["NONE","DRAGON3","BIN","-R U -g 1"],
["NONE","DRAGON5","SMD","-R U -g 1"],
["NONE","DRAGON","SMD","-R U -g 1"],
["NONE","DREVENGE","SMD","-R U -g 1"],
["NONE","DRROBOT","BIN","-R U -g 1"],
["NONE","DTRACY","SMD","-R U -g 1"],
["NONE","DUNE2","BIN","-R U -g 1"],
["NONE","DYNAMITD","SMD","-R U -g 1"],
["NONE","ECCO2","BIN","-R U -g 1"],
["NONE","ECCO","BIN","-R U -g 1"],
["NONE","EEVANS","SMD","-R U -g 1"],
["NONE","EHOLYF","SMD","-R U -g 1"],
["NONE","ELIMDOWN","SMD","-R U -g 1"],
["NONE","ELVENTO","SMD","-R U -g 1"],
["NONE","EMASTER","SMD","-R U -g 1"],
["NONE","ESCAPE","SMD","-R U -g 1"],
["NONE","ESPNBB","BIN","-R U -g 1"],
["NONE","E-SWAT","SMD","-R U -g 1"],
["NONE","ETERNAL","BIN","-R U -g 1"],
["NONE","EVANS","SMD","-R U -g 1"],
["NONE","EWJ2","SMD","-R U -g 1"],
["NONE","EWJ","SMD","-R U -g 1"],
["NONE","EXILE","BIN","-R U -g 1"],
["NONE","EXMUTANT","SMD","-R U -g 1"],
["NONE","EXOSQUAD","SMD","-R U -g 1"],
["NONE","F15EAGLE","SMD","-R U -g 1"],
["NONE","F22-INT","SMD","-R U -g 1"],
["NONE","FANTASIA","SMD","-R U -g 1"],
["NONE","FARYTALE","BIN","-R U -g 1"],
["NONE","FATAL2","SMD","-R U -g 1"],
["NONE","FATALLB","SMD","-R U -g 1"],
["NONE","FATALREW","SMD","-R U -g 1"],
["NONE","FFURY","BIN","-R U -g 1"],
["NONE","FIFA96","SMD","-R U -g 1"],
["NONE","FIFA97","SMD","-R U -g 1"],
["NONE","FIFA","SMD","-R U -g 1"],
["NONE","FISTNS","SMD","-R U -g 1"],
["NONE","FLASHBAC","BIN","-R U -g 1"],
["NONE","FLICKY","SMD","-R U -g 1"],
["NONE","FLINK","SMD","-R U -g 1"],
["NONE","FREAL","SMD","-R U -g 1"],
["NONE","FSTONES","SMD","-R U -g 1"],
["NONE","FUNCAR","SMD","-R U -g 1"],
["NONE","F-WORLDS","SMD","-R U -g 1"],
["NONE","FZONE","SMD","-R U -g 1"],
["NONE","GAIARES","BIN","-R U -g 1"],
["NONE","GAING","SMD","-R U -g 1"],
["NONE","GARFIELD","BIN","-R U -g 1"],
["NONE","GAUNTLT4","BIN","-R U -g 1"],
["NONE","GEMFIRE","BIN","-R U -g 1"],
["NONE","GHOSTBST","SMD","-R U -g 1"],
["NONE","GHOULSG","BIN","-R U -g 1"],
["NONE","GLEYLANC","SMD","-R U -g 1"],
["NONE","G-LOC","SMD","-R U -g 1"],
["NONE","GNCHAOS","SMD","-R U -g 1"],
["NONE","GODS","SMD","-R U -g 1"],
["NONE","GOLDEN2","SMD","-R U -g 1"],
["NONE","GOLDEN3","SMD","-R U -g 1"],
["NONE","GOLDEN","SMD","-R U -g 1"],
["NONE","GOOFY","BIN","-R U -g 1"],
["NONE","GREATEST","SMD","-R U -g 1"],
["NONE","GREENDOG","BIN","-R U -g 1"],
["NONE","GRINDSTR","BIN","-R U -g 1"],
["NONE","GROWL","BIN","-R U -g 1"],
["NONE","GUNSTAR","SMD","-R U -g 1"],
["NONE","GYNOUG","SMD","-R U -g 1"],
["NONE","HANGON","BIN","-R U -g 1"],
["NONE","HARDBALL","BIN","-R U -g 1"],
["NONE","HARDDRIV","BIN","-R U -g 1"],
["NONE","HARDWIRE","BIN","-R U -g 1"],
["NONE","HARRIER2","SMD","-R U -g 1"],
["NONE","H_DUNK","BIN","-R U -g 1"],
["NONE","HELLFIRE","SMD","-R U -g 1"],
["NONE","HERZOG","SMD","-R U -g 1"],
["NONE","HIGH_SEA","BIN","-R U -g 1"],
["NONE","HOMEALON","SMD","-R U -g 1"],
["NONE","IHULK","SMD","-R U -g 1"],
["NONE","IMGTENN","SMD","-R U -g 1"],
["NONE","IMMORTAL","SMD","-R U -g 1"],
["NONE","INDYCRUS","SMD","-R U -g 1"],
["NONE","INSECTRX","SMD","-R U -g 1"],
["NONE","ITCHY","SMD","-R U -g 1"],
["NONE","JEOPARDY","BIN","-R U -g 1"],
["NONE","JOEMONT2","SMD","-R U -g 1"],
["NONE","JPOND2","SMD","-R U -g 1"],
["NONE","JPOND3","SMD","-R U -g 1"],
["NONE","JPOND","SMD","-R U -g 1"],
["NONE","JSTRIKE","SMD","-R U -g 1"],
["NONE","JUNGBOOK","SMD","-R U -g 1"],
["NONE","JURASSI2","SMD","-R U -g 1"],
["NONE","JURASSIC","SMD","-R U -g 1"],
["NONE","KBOUNTY","SMD","-R U -g 1"],
["NONE","KIDCHA","SMD","-R U -g 1"],
["NONE","KINGDOM","SMD","-R U -g 1"],
["NONE","KINGMONS","SMD","-R U -g 1"],
["NONE","KINGSALM","SMD","-R U -g 1"],
["NONE","KLAX","SMD","-R U -g 1"],
["NONE","LANDSTA","BIN","-R U -g 1"],
["NONE","LASTBTLE","SMD","-R U -g 1"],
["NONE","LETHAL2","BIN","-R U -g 1"],
["NONE","LETHAL","SMD","-R U -g 1"],
["NONE","LIGHT","SMD","-R U -g 1"],
["NONE","LIONKING","SMD","-R U -g 1"],
["NONE","LOSTWRLD","SMD","-R U -g 1"],
["NONE","MADDEN97","SMD","-R U -g 1"],
["NONE","MAGILHAT","SMD","-R U -g 1"],
["NONE","MAOU","SMD","-R U -g 1"],
["NONE","MARBLE","SMD","-R U -g 1"],
["NONE","MARIOHOC","SMD","-R U -g 1"],
["NONE","MARVLAND","SMD","-R U -g 1"],
["NONE","MASMON","BIN","-R U -g 1"],
["NONE","MAXIMUM","SMD","-R U -g 1"],
["NONE","MBOMBER","SMD","-R U -g 1"],
["NONE","MCKIDS","SMD","-R U -g 1"],
["NONE","MEGAMAN","SMD","-R U -g 1"],
["NONE","MEGATURR","SMD","-R U -g 1"],
["NONE","MERCS","SMD","-R U -g 1"],
["NONE","MICKEYMA","SMD","-R U -g 1"],
["NONE","MICKEY","SMD","-R U -g 1"],
["NONE","MICKY'S","SMD","-R U -g 1"],
["NONE","MICROMAC","SMD","-R U -g 1"],
["NONE","MIDNIGHT","BIN","-R U -g 1"],
["NONE","MK2","BIN","-R U -g 1"],
["NONE","MK","BIN","-R U -g 1"],
["NONE","MLBPA","BIN","-R U -g 1"],
["NONE","MLEAGUE","SMD","-R U -g 1"],
["NONE","M&M3","BIN","-R U -g 1"],
["NONE","M&M","BIN","-R U -g 1"],
["NONE","MMNSTRS","SMD","-R U -g 1"],
["NONE","MONOPOLY","SMD","-R U -g 1"],
["NONE","MOONWALK","SMD","-R U -g 1"],
["NONE","MPAINEL","SMD","-R U -g 1"],
["NONE","MRNUTZ","SMD","-R U -g 1"],
["NONE","MSPACMAN","BIN","-R U -g 1"],
["NONE","MUTANTAN","SMD","-R U -g 1"],
["NONE","MYSTICAL","SMD","-R U -g 1"],
["NONE","MYSTIC-D","SMD","-R U -g 1"],
["NONE","NBAHANG","SMD","-R U -g 1"],
["NONE","NBAJAM","BIN","-R U -g 1"],
["NONE","NEWZEAL","SMD","-R U -g 1"],
["NONE","NHL92","BIN","-R U -g 1"],
["NONE","NHL93","SMD","-R U -g 1"],
["NONE","NHL94","BIN","-R U -g 1"],
["NONE","NHL_95","BIN","-R U -g 1"],
["NONE","NITEMARE","BIN","-R U -g 1"],
["NONE","NJGAIDEN","BIN","-R U -g 1"],
["NONE","ODYSEY","BIN","-R U -g 1"],
["NONE","OPEREURO","BIN","-R U -g 1"],
["NONE","OUTLAND","SMD","-R U -g 1"],
["NONE","OUTRUN","SMD","-R U -g 1"],
["NONE","OUTWORLD","SMD","-R U -g 1"],
["NONE","PACATTAC","BIN","-R U -g 1"],
["NONE","PACMAN2","SMD","-R U -g 1"],
["NONE","PACMANIA","BIN","-R U -g 1"],
["NONE","PAPRBOY2","SMD","-R U -g 1"],
["NONE","PGA96","SMD","-R U -g 1"],
["NONE","PHELIOS","SMD","-R U -g 1"],
["NONE","PIRATDW","SMD","-R U -g 1"],
["NONE","PITFALL","SMD","-R U -g 1"],
["NONE","PITFIGHT","BIN","-R U -g 1"],
["NONE","POCAHONT","SMD","-R U -g 1"],
["NONE","POPULUS2","SMD","-R U -g 1"],
["NONE","POPULUS","SMD","-R U -g 1"],
["NONE","POWRANGM","SMD","-R U -g 1"],
["NONE","PRIMRAGE","SMD","-R U -g 1"],
["NONE","PRINCE","SMD","-R U -g 1"],
["NONE","PSTAR2","SMD","-R U -g 1"],
["NONE","PSTAR3","BIN","-R U -g 1"],
["NONE","PSTAR4","SMD","-R U -g 1"],
["NONE","PSYCHO","SMD","-R U -g 1"],
["NONE","PUGGSY","SMD","-R U -g 1"],
["NONE","PULSEMAN","SMD","-R U -g 1"],
["NONE","PUNISHER","SMD","-R U -g 1"],
["NONE","PUYO","SMD","-R U -g 1"],
["NONE","QUACKSHT","SMD","-R U -g 1"],
["NONE","RACEDRIV","SMD","-R U -g 1"],
["NONE","RAGNACE","BIN","-R U -g 1"],
["NONE","RAIDEN","SMD","-R U -g 1"],
["NONE","RAMBO3","SMD","-R U -g 1"],
["NONE","RAMPART","SMD","-R U -g 1"],
["NONE","RANGERX","SMD","-R U -g 1"],
["NONE","RASTAN2","SMD","-R U -g 1"],
["NONE","RBI4","SMD","-R U -g 1"],
["NONE","RCPROAM","SMD","-R U -g 1"],
["NONE","RESQ","BIN","-R U -g 1"],
["NONE","RINGOPWR","BIN","-R U -g 1"],
["NONE","RISLAND","SMD","-R U -g 1"],
["NONE","RISTAR","SMD","-R U -g 1"],
["NONE","ROADRAS2","SMD","-R U -g 1"],
["NONE","ROADRAS3","SMD","-R U -g 1"],
["NONE","ROADRAS","SMD","-R U -g 1"],
["NONE","ROBOVSTE","SMD","-R U -g 1"],
["NONE","ROCKNIT","SMD","-R U -g 1"],
["NONE","ROLLING2","SMD","-R U -g 1"],
["NONE","ROLO","SMD","-R U -g 1"],
["NONE","ROMANCE2","SMD","-R U -g 1"],
["NONE","ROMANCE3","SMD","-R U -g 1"],
["NONE","ROYLRMBL","SMD","-R U -g 1"],
["NONE","RROLLRAC","BIN","-R U -g 1"],
["NONE","RSHINOBI","SMD","-R U -g 1"],
["NONE","RSTIMP-I","SMD","-R U -g 1"],
["NONE","RUGBY95","SMD","-R U -g 1"],
["NONE","SAILORM","SMD","-R U -g 1"],
["NONE","S_BEAST","SMD","-R U -g 1"],
["NONE","SDARKNES","SMD","-R U -g 1"],
["NONE","SECSAMUR","SMD","-R U -g 1"],
["NONE","SF2TURBO","BIN","-R U -g 1"],
["NONE","SFLIGHT","SMD","-R U -g 1"],
["NONE","SFORCE1","SMD","-R U -g 1"],
["NONE","SFORCE2","SMD","-R U -g 1"],
["NONE","SFORCE","SMD","-R U -g 1"],
["NONE","SHADOWRN","SMD","-R U -g 1"],
["NONE","SHAQFU","BIN","-R U -g 1"],
["NONE","SHINOBI2","SMD","-R U -g 1"],
["NONE","SHINOBI3","BIN","-R U -g 1"],
["NONE","SHOVEIT","SMD","-R U -g 1"],
["NONE","SIDEPOCK","SMD","-R U -g 1"],
["NONE","SMASHTV","SMD","-R U -g 1"],
["NONE","SNOOKER","BIN","-R U -g 1"],
["NONE","SNOWBROS","SMD","-R U -g 1"],
["NONE","SOLEIL","BIN","-R U -g 1"],
["NONE","SONIC1","BIN","-R U -g 1"],
["NONE","SONIC2","SMD","-R U -g 1"],
["NONE","SONIC3D","SMD","-R U -g 1"],
["NONE","SONIC3","SMD","-R U -g 1"],
["NONE","SONICK","BIN","-R U -g 1"],
["NONE","SONICSPN","BIN","-R U -g 1"],
["NONE","SPARKSTE","SMD","-R U -g 1"],
["NONE","SPBALL2","SMD","-R U -g 1"],
["NONE","SPEEDYGZ","SMD","-R U -g 1"],
["NONE","SPIDERMN","SMD","-R U -g 1"],
["NONE","SPIDXMEN","SMD","-R U -g 1"],
["NONE","SPLATR2","SMD","-R U -g 1"],
["NONE","SPLATR3","SMD","-R U -g 1"],
["NONE","SRROLL","SMD","-R U -g 1"],
["NONE","SSHODOWN","SMD","-R U -g 1"],
["NONE","SSMART","SMD","-R U -g 1"],
["NONE","STARCONT","SMD","-R U -g 1"],
["NONE","STEELEMP","SMD","-R U -g 1"],
["NONE","STIMPY","SMD","-R U -g 1"],
["NONE","STNEXTG","BIN","-R U -g 1"],
["NONE","STORMLRD","SMD","-R U -g 1"],
["NONE","STREET2","SMD","-R U -g 1"],
["NONE","STREET3","SMD","-R U -g 1"],
["NONE","STREET","BIN","-R U -g 1"],
["NONE","STREETRA","SMD","-R U -g 1"],
["NONE","STRIDER2","SMD","-R U -g 1"],
["NONE","STRIDER","BIN","-R U -g 1"],
["NONE","SUBTERRA","SMD","-R U -g 1"],
["NONE","SUMMER","SMD","-R U -g 1"],
["NONE","SUNSET","SMD","-R U -g 1"],
["NONE","SUPERMAN","SMD","-R U -g 1"],
["NONE","SWORDVRM","BIN","-R U -g 1"],
["NONE","SW-SODAN","SMD","-R U -g 1"],
["NONE","SYDVALIS","SMD","-R U -g 1"],
["NONE","T2ARCADE","SMD","-R U -g 1"],
["NONE","T2D","SMD","-R U -g 1"],
["NONE","TALULUTO","SMD","-R U -g 1"],
["NONE","TEARTH","SMD","-R U -g 1"],
["NONE","TECBASE","BIN","-R U -g 1"],
["NONE","TECBOWL2","SMD","-R U -g 1"],
["NONE","TECBOWL3","SMD","-R U -g 1"],
["NONE","TECBOWL","SMD","-R U -g 1"],
["NONE","TECHNOC","BIN","-R U -g 1"],
["NONE","TETRIS","BIN","-R U -g 1"],
["NONE","TFHARRY","SMD","-R U -g 1"],
["NONE","T-FORCE2","SMD","-R U -g 1"],
["NONE","T-FORCE3","SMD","-R U -g 1"],
["NONE","T-FORCE4","SMD","-R U -g 1"],
["NONE","THOR","BIN","-R U -g 1"],
["NONE","TICK","SMD","-R U -g 1"],
["NONE","TMNT4","SMD","-R U -g 1"],
["NONE","TOEJAM2","SMD","-R U -g 1"],
["NONE","TOEJAM","BIN","-R U -g 1"],
["NONE","TOKI","SMD","-R U -g 1"],
["NONE","TOUGHMAN","SMD","-R U -g 1"],
["NONE","TOUTRUN","SMD","-R U -g 1"],
["NONE","TOYSTORY","SMD","-R U -g 1"],
["NONE","TRATERRO","SMD","-R U -g 1"],
["NONE","TRAYSIA","BIN","-R U -g 1"],
["NONE","TRUXTON","SMD","-R U -g 1"],
["NONE","TTOONS","SMD","-R U -g 1"],
["NONE","TWINHAWK","SMD","-R U -g 1"],
["NONE","TYRANTS","SMD","-R U -g 1"],
["NONE","ULTRAMAN","SMD","-R U -g 1"],
["NONE","UMK3","SMD","-R U -g 1"],
["NONE","UNDEAD","BIN","-R U -g 1"],
["NONE","USTRIKE","SMD","-R U -g 1"],
["NONE","VALIS3","SMD","-R U -g 1"],
["NONE","VALIS","BIN","-R U -g 1"],
["NONE","VAPRTRAI","SMD","-R U -g 1"],
["NONE","VBART","SMD","-R U -g 1"],
["NONE","VECTOR2","SMD","-R U -g 1"],
["NONE","VECTOR","SMD","-R U -g 1"],
["NONE","VF2","SMD","-R U -g 1"],
["NONE","VIEWPT","SMD","-R U -g 1"],
["NONE","VOLLEY","SMD","-R U -g 1"],
["NONE","WACKYRAC","BIN","-R U -g 1"],
["NONE","WALDO","BIN","-R U -g 1"],
["NONE","WARDNER","SMD","-R U -g 1"],
["NONE","WARLOCK","SMD","-R U -g 1"],
["NONE","WARRIORS","SMD","-R U -g 1"],
["NONE","WARSONG","SMD","-R U -g 1"],
["NONE","WBOY4J","BIN","-R U -g 1"],
["NONE","WEREBACK","BIN","-R U -g 1"],
["NONE","WHIPRUSH","BIN","-R U -g 1"],
["NONE","WILUSION","SMD","-R U -g 1"],
["NONE","WINBLEDM","SMD","-R U -g 1"],
["NONE","WINTER","SMD","-R U -g 1"],
["NONE","WIZNLIZ","SMD","-R U -g 1"],
["NONE","WOLVERIN","SMD","-R U -g 1"],
["NONE","WONDER3","BIN","-R U -g 1"],
["NONE","WONDER5","BIN","-R U -g 1"],
["NONE","WRESTWAR","SMD","-R U -g 1"],
["NONE","WROME2","BIN","-R U -g 1"],
["NONE","WROME","BIN","-R U -g 1"],
["NONE","WSBBAL98","BIN","-R U -g 1"],
["NONE","WWF_WM","SMD","-R U -g 1"],
["NONE","WWFWM","SMD","-R U -g 1"],
["NONE","XENON2","BIN","-R U -g 1"],
["NONE","XMEN2","SMD","-R U -g 1"],
["NONE","XMEN","BIN","-R U -g 1"],
["NONE","YOCHI","SMD","-R U -g 1"],
["NONE","YS3","SMD","-R U -g 1"],
["NONE","ZERO","SMD","-R U -g 1"],
["NONE","ZEROTOL","SMD","-R U -g 1"],
["NONE","ZEROWING","SMD","-R U -g 1"],
["NONE","ZOMBIE","SMD","-R U -g 1"],
["NONE","ZOOL","SMD","-R U -g 1"],
["NONE","ZOOM","SMD","-R U -g 1"]]
